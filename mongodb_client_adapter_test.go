package olmomongodb_test

import (
	"context"
	"testing"

	mongodb "bitbucket.org/olmo-technology-corp/olmomongodb"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo"
)

func Test_ClientAdapter_Connect(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})

	err := client.Connect(context.Background())

	assert.NoError(t, err, "Should not error")
}

func Test_ClientAdapter_Disconnect(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})

	err := client.Disconnect(context.Background())

	assert.NoError(t, err, "Should not error")
}

func Test_ClientAdapter_Database(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})

	db := client.Database("test")

	assert.NotNil(t, db, "Should not nill")
}
