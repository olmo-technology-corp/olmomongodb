package olmomongodb_test

import (
	"context"
	"testing"
	"time"

	mongodb "bitbucket.org/olmo-technology-corp/olmomongodb"
	"github.com/stretchr/testify/assert"
	"go.elastic.co/apm/module/apmmongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func Test_CollectionAdapter_FindOne(t *testing.T) {
	// client := new(mocks.Client)
	mca := mongodb.NewClientAdapter(&mongo.Client{})
	mca.Connect(context.Background())

	db := mca.Database("test")

	var colAdap mongodb.Collection = db.Collection("col-test")

	res := colAdap.FindOne(context.Background(), nil)

	assert.NotNil(t, res, "Result should not be nil")
}

func Test_CollectionAdapter_InsertOne(t *testing.T) {
	// client := new(mocks.Client)
	opts := options.Client().
		ApplyURI("mongodb://test:27017/testdb").
		SetMonitor(apmmongo.CommandMonitor()).
		SetMaxPoolSize(1000).
		SetRetryWrites(true).
		SetRetryReads(true)

	cl, _ := mongo.NewClient(opts)
	// cl, _ := mongo.NewClient()

	clientAdapter := mongodb.NewClientAdapter(cl)
	clientAdapter.Connect(context.Background())

	dbAdapter := clientAdapter.Database("db-test")

	colAdap := dbAdapter.Collection("col-test")

	testData := map[string]interface{}{
		"tes": "01",
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Microsecond*1)
	defer cancel()
	res, err := colAdap.InsertOne(ctx, testData)

	assert.Error(t, err, "Error should not nil")
	assert.Nil(t, res, "Resulr should nil")
}

func Test_CollectionAdapter_Find(t *testing.T) {
	opts := options.Client().
		ApplyURI("mongodb://test:27017/testdb").
		SetMonitor(apmmongo.CommandMonitor()).
		SetMaxPoolSize(1000).
		SetRetryWrites(true).
		SetRetryReads(true)

	cl, _ := mongo.NewClient(opts)

	clientAdapter := mongodb.NewClientAdapter(cl)
	clientAdapter.Connect(context.Background())

	dbAdapter := clientAdapter.Database("db-test")

	colAdap := dbAdapter.Collection("col-test")

	ctx, cancel := context.WithTimeout(context.Background(), time.Microsecond*1)
	defer cancel()
	res, err := colAdap.Find(ctx, bson.M{})

	assert.Error(t, err, "Error should not nil")
	assert.Nil(t, res, "Resulr should nil")
}

func Test_CollectionAdapter_InsertMany(t *testing.T) {
	opts := options.Client().
		ApplyURI("mongodb://test:27017/testdb").
		SetMonitor(apmmongo.CommandMonitor()).
		SetMaxPoolSize(1000).
		SetRetryWrites(true).
		SetRetryReads(true)

	cl, _ := mongo.NewClient(opts)

	clientAdapter := mongodb.NewClientAdapter(cl)
	clientAdapter.Connect(context.Background())

	dbAdapter := clientAdapter.Database("db-test")

	colAdap := dbAdapter.Collection("col-test")

	testData := []interface{}{}

	ctx, cancel := context.WithTimeout(context.Background(), time.Microsecond*1)
	defer cancel()
	res, err := colAdap.InsertMany(ctx, testData)

	assert.Error(t, err, "Error should not nil")
	assert.Nil(t, res, "Resulr should nil")
}

func Test_CollectionAdapter_CountDocuments(t *testing.T) {
	opts := options.Client().
		ApplyURI("mongodb://test:27017/testdb").
		SetMonitor(apmmongo.CommandMonitor()).
		SetMaxPoolSize(1000).
		SetRetryWrites(true).
		SetRetryReads(true)

	cl, _ := mongo.NewClient(opts)

	clientAdapter := mongodb.NewClientAdapter(cl)
	clientAdapter.Connect(context.Background())

	dbAdapter := clientAdapter.Database("db-test")

	colAdap := dbAdapter.Collection("col-test")

	ctx, cancel := context.WithTimeout(context.Background(), time.Microsecond*1)
	defer cancel()
	res, err := colAdap.CountDocuments(ctx, bson.M{})

	assert.Error(t, err, "Error should not nil")
	assert.Zero(t, res, "Resulr should zero")
}

func Test_CollectionAdapter_DeleteOne(t *testing.T) {
	opts := options.Client().
		ApplyURI("mongodb://test:27017/testdb").
		SetMonitor(apmmongo.CommandMonitor()).
		SetMaxPoolSize(1000).
		SetRetryWrites(true).
		SetRetryReads(true)

	cl, _ := mongo.NewClient(opts)

	clientAdapter := mongodb.NewClientAdapter(cl)
	clientAdapter.Connect(context.Background())

	dbAdapter := clientAdapter.Database("db-test")

	colAdap := dbAdapter.Collection("col-test")

	ctx, cancel := context.WithTimeout(context.Background(), time.Microsecond*1)
	defer cancel()
	res, err := colAdap.DeleteOne(ctx, bson.M{})

	assert.Error(t, err, "Error should not nil")
	assert.Nil(t, res, "Resulr should nil")
}

func Test_CollectionAdapter_DeleteMany(t *testing.T) {
	opts := options.Client().
		ApplyURI("mongodb://test:27017/testdb").
		SetMonitor(apmmongo.CommandMonitor()).
		SetMaxPoolSize(1000).
		SetRetryWrites(true).
		SetRetryReads(true)

	cl, _ := mongo.NewClient(opts)

	clientAdapter := mongodb.NewClientAdapter(cl)
	clientAdapter.Connect(context.Background())

	dbAdapter := clientAdapter.Database("db-test")

	colAdap := dbAdapter.Collection("col-test")

	ctx, cancel := context.WithTimeout(context.Background(), time.Microsecond*1)
	defer cancel()
	res, err := colAdap.DeleteMany(ctx, bson.M{})

	assert.Error(t, err, "Error should not nil")
	assert.Nil(t, res, "Resulr should nil")
}

func Test_CollectionAdapter_UpdateOne(t *testing.T) {
	opts := options.Client().
		ApplyURI("mongodb://test:27017/testdb").
		SetMonitor(apmmongo.CommandMonitor()).
		SetMaxPoolSize(1000).
		SetRetryWrites(true).
		SetRetryReads(true)

	cl, _ := mongo.NewClient(opts)

	clientAdapter := mongodb.NewClientAdapter(cl)
	clientAdapter.Connect(context.Background())

	dbAdapter := clientAdapter.Database("db-test")

	colAdap := dbAdapter.Collection("col-test")

	ctx, cancel := context.WithTimeout(context.Background(), time.Microsecond*1)
	defer cancel()
	res, err := colAdap.UpdateOne(ctx, bson.M{}, bson.M{})

	assert.Error(t, err, "Error should not nil")
	assert.Nil(t, res, "Resulr should nil")
}

func Test_CollectionAdapter_UpdateMany(t *testing.T) {
	opts := options.Client().
		ApplyURI("mongodb://test:27017/testdb").
		SetMonitor(apmmongo.CommandMonitor()).
		SetMaxPoolSize(1000).
		SetRetryWrites(true).
		SetRetryReads(true)

	cl, _ := mongo.NewClient(opts)

	clientAdapter := mongodb.NewClientAdapter(cl)
	clientAdapter.Connect(context.Background())

	dbAdapter := clientAdapter.Database("db-test")

	colAdap := dbAdapter.Collection("col-test")

	ctx, cancel := context.WithTimeout(context.Background(), time.Microsecond*1)
	defer cancel()
	res, err := colAdap.UpdateMany(ctx, bson.M{}, bson.M{})

	assert.Error(t, err, "Error should not nil")
	assert.Nil(t, res, "Resulr should nil")
}
