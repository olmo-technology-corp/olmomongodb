package olmomongodb_test

import (
	"testing"

	mongodb "bitbucket.org/olmo-technology-corp/olmomongodb"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo"
)

func Test_MongoDatabaseAdapter_Collection(t *testing.T) {
	mca := mongodb.NewClientAdapter(&mongo.Client{})

	db := mca.Database("db-test")

	c := db.Collection("test")

	assert.NotNil(t, c, "Collection should not be nil")
}
