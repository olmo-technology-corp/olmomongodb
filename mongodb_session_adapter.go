package olmomongodb

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type SessionAdapter struct {
	session mongo.Session
}

func (s *SessionAdapter) StartTransaction(opts ...*options.TransactionOptions) (err error) {
	err = s.session.StartTransaction(opts...)
	return
}

func (s *SessionAdapter) AbortTransaction(ctx context.Context) (err error) {
	err = s.session.AbortTransaction(ctx)
	return
}

func (s *SessionAdapter) CommitTransaction(ctx context.Context) (err error) {
	err = s.session.CommitTransaction(ctx)
	return
}

func (s *SessionAdapter) WithTransaction(ctx context.Context, fn func(sessCtx mongo.SessionContext) (interface{}, error),
	opts ...*options.TransactionOptions) (it interface{}, err error) {
	it, err = s.session.WithTransaction(ctx, fn, opts...)
	return
}

func (s *SessionAdapter) EndSession(ctx context.Context) {
	s.session.EndSession(ctx)
}

func (s *SessionAdapter) ID() (r bson.Raw) {
	r = s.session.ID()
	return
}
