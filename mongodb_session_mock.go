package olmomongodb

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type SessionMock struct{}

func (s SessionMock) StartTransaction(...*options.TransactionOptions) (err error) {
	return
}

func (s SessionMock) AbortTransaction(context.Context) (err error) {
	return
}

func (s SessionMock) CommitTransaction(context.Context) (err error) {
	return
}

func (s SessionMock) WithTransaction(ctx context.Context, fn func(sessCtx mongo.SessionContext) (interface{}, error),
	opts ...*options.TransactionOptions) (it interface{}, err error) {
	it, err = fn(nil)
	return
}

func (s SessionMock) EndSession(context.Context) {
}

func (s SessionMock) ID() (r bson.Raw) {
	return
}
